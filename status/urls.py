from django.urls import path
from . import views

appname = 'status'

urlpatterns= [
    path('', views.index, name='index'),
    path('confirmation/', views.confirmation, name='confirmation'),
    path('<int:pk>/', views.remove, name='remove')
]