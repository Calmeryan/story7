from django.test import TestCase, Client
from django.urls import resolve
from .views import index, confirmation, remove
from .models import Status
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
from webdriver_manager.chrome import ChromeDriverManager

# Create your tests here.
class TestCaseStatus(TestCase):

    def test_landing_page_check(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_confirmation_page_check(self):
        found = resolve('/confirmation/')
        self.assertEqual(found.func, confirmation)

    def test_page_after_remove_object(self):
        response = Client().get('/1/')
        self.assertEqual(response.status_code, 200)

    def test_story7_use_func_index(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_model_status(self):
        Status.objects.create(name="thau", message="baik")
        counter = Status.objects.all().count()
        self.assertEqual(counter, 1)

    def test_form_is_submitted(self):
        response = self.client.post('/', follow=True, data={
            'name' : 'thau',
            'message' : 'baik'
        })
        self.assertEqual(Status.objects.count(), 1)

    def test_form_is_submitted_2(self):
        response = self.client.post('/1/', follow=True, data={
            'name' : 'thau',
            'message' : 'baik'
        })
        self.assertEqual(Status.objects.count(), 1)

    def test_func_remove_is_used(self):
        found = resolve('/1/')
        self.assertEqual(found.func, remove)

class FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()

    def test_input_status_yes(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        time.sleep(4)
        name = selenium.find_element_by_id('id_name')
        time.sleep(2)
        message = selenium.find_element_by_id('id_message')
        time.sleep(2)

        name.send_keys("Calme")
        time.sleep(2)
        message.send_keys("PPW asik!")
        time.sleep(2)

        selenium.find_element_by_css_selector("input[name='submit'][value='Submit']").click()

        selenium.get('http://127.0.0.1:8000/confirmation')
        time.sleep(3)

        yes = selenium.find_element_by_name("Yes")
        yes.send_keys(Keys.RETURN)
        time.sleep(4)

        selenium.get('http://127.0.0.1:8000/')

    def test_input_status_no(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        time.sleep(4)
        name = selenium.find_element_by_id('id_name')
        time.sleep(2)
        message = selenium.find_element_by_id('id_message')
        time.sleep(2)

        name.send_keys("Ryan")
        time.sleep(2)
        message.send_keys("Asik Banget!")
        time.sleep(2)

        selenium.find_element_by_css_selector("input[name='submit'][value='Submit']").click()

        selenium.get('http://127.0.0.1:8000/confirmation')
        time.sleep(3)

        no = selenium.find_element_by_name("No")
        no.send_keys(Keys.RETURN)
        time.sleep(4)

        selenium.get('http://127.0.0.1:8000/')



        


    