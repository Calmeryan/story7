from django import forms

class formStatus(forms.Form):
    name = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Name',
        'type' : 'text',
        'required' : True
    }))

    message = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Your message',
        'type' : 'text',
        'required' : True
    }))