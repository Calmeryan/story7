from django.shortcuts import render, redirect
from .import forms, models

# Create your views here.
def index(request):
    if request.method == "POST":
        isi = forms.formStatus(request.POST)
        if isi.is_valid():
            update = models.Status()
            update.name = isi.cleaned_data["name"]
            update.message = isi.cleaned_data["message"]
            update.save()
        return redirect("/confirmation")

    else:
        isi = forms.formStatus()
        objek_isi = models.Status.objects.all()
        isi_dict = {
            'forms' : isi,
            'isi' : objek_isi   
        }
        return render(request, 'home.html', isi_dict)

def confirmation(request):
    confirm = models.Status.objects.last()
    confirm_dict = {
        'isi' : confirm
    }
    return render(request, 'confirmation.html', confirm_dict)

def remove(request, pk):
    if request.method == "POST":
        isi = forms.formStatus(request.POST)
        if isi.is_valid():
            update = models.Status()
            update.name = isi.cleaned_data["name"]
            update.message = isi.cleaned_data["message"]
            update.save()
        return redirect("/confirmation")

    else:
        models.Status.objects.filter(pk = pk).delete()
        isi = forms.formStatus()
        objek_isi = models.Status.objects.all()
        isi_dict = {
            'forms' : isi,
            'isi' : objek_isi   
        }
        return render(request, 'home.html', isi_dict)
    
